# Signal Sticker Extractor

A simple Python script to extract a [Signal](https://signal.org) sticker pack
and write the stickers as images in a local directory.

## How to install?

The script obviously requires Python. Although there aren’t any test, it’s
supposed to work with any recent version of Python (> 3.7). A few Python
dependencies are required, and it’s advised to install those in a virtual
environment.

How to install on Windows:

```shell
git clone https://framagit.org/vegaelle/signalstickers_extract.git
cd signalstickers_extract
py -3 -m venv venv
.\venv\Scripts\activate.bat
pip install -r requirements.txt
```

On a UNIX/Linux system:

```shell
git clone https://framagit.org/vegaelle/signalstickers_extract.git
cd signalstickers_extract
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## How to use?

First, get your pack ID and key. If you’re browsing https://signalstickers.com,
those 2 informations are in the "Add to Signal" link.

So, if your sticker pack is "cats" from
https://signalstickers.com/pack/8d7c881d84fe6e69836369919e729fb2, then your
pack ID is present in the URL (`8d7c881d84fe6e69836369919e729fb2`) and the pack
key is in the "Add to Signal" link: `1ce99a259b9d626d01b509e098458fb5db6c81e6752a2a406e590ff51ec71946`.

So you just have to run the following command:

``` shell
python main.py 8d7c881d84fe6e69836369919e729fb2 1ce99a259b9d626d01b509e098458fb5db6c81e6752a2a406e590ff51ec71946
```

And your sticker images will be in the `stickers` directory. Each sticker will
be named after its emoji. If multiple stickers have the same emoji, a number
will be appended to each one.
