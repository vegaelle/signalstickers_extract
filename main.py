import os
import sys
import argparse
import unicodedata
import re
import itertools
import anyio
from signalstickers_client import StickersClient
import emoji
import progressbar
from contextlib import contextmanager

def slugify(value, allow_unicode=False):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.

    Source: Django Project
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')


@contextmanager
def noop():
    yield


async def main():

    parser = argparse.ArgumentParser(description="Download Signal stickers from a pack")
    parser.add_argument(
        'pack_id',
        help='Sticker pack ID (found in signalstickers.com or signal.art URL)'
    )
    parser.add_argument(
        'pack_key',
        help='Sticker pack key (found in signal.art URL)'
    )
    parser.add_argument(
        '-p', '--progress',
        action='store_true',
        help='Displays progress')
    args = parser.parse_args()


    async with StickersClient() as client:
        pack = await client.get_pack(args.pack_id, args.pack_key)

    pack_name = slugify(pack.title)

    async def save_sticker(sticker, processed, idx=None):
        emoji_name = emoji.demojize(sticker.emoji, delimiters=('', ''))
        if idx is None:
            emoji_file = os.path.join(
                "stickers", pack_name,
                "{}.webp".format(slugify(emoji_name)))
        else:
            emoji_file = os.path.join(
                "stickers", pack_name,
                "{}_{}.webp".format(slugify(emoji_name), idx))
        async with await anyio.open_file(emoji_file, "wb") as f:
            await f.write(sticker.image_data)
            processed.append(sticker.id)
            if args.progress:
                bar.update(len(processed))

    async with anyio.create_task_group() as tg:
        try:
            if not os.path.exists("stickers"):
                os.mkdir("stickers")
            if not os.path.exists(os.path.join("stickers", pack_name)):
                os.mkdir(os.path.join("stickers", pack_name))
        except OSError as e:
            print(f"Unable to create sticker pack dir: {e}", file=sys.stderr)
            sys.exit(1)
        if args.progress:
            bar_context = progressbar.ProgressBar(max_value=len(pack.stickers))
        else:
            bar_context = noop()
        with bar_context as bar:
            processed_stickers = []
            for _, stickers in itertools.groupby(pack.stickers, lambda s: s.emoji):
                stickers = list(stickers)
                if len(stickers) == 1:
                    sticker = stickers[0]
                    await tg.spawn(save_sticker, sticker, processed_stickers)
                else:
                    for idx, sticker in enumerate(stickers):
                        await tg.spawn(save_sticker, sticker,
                                processed_stickers, idx)
 

if __name__ == '__main__':
    anyio.run(main)
